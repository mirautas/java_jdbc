package net.netconomy.jdbc;

public class Student {
    private int id;
    private String first_name;
    private String last_name;
    private int grade;
    private String email;
    private boolean hasSchoolarship;

    public Student() {

    }

    public Student(String first_name, String last_name, int grade, String email, boolean hasSchoolarship) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.grade = grade;
        this.email = email;
        this.hasSchoolarship = hasSchoolarship;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getHasSchoolarship() {
        return hasSchoolarship;
    }

    public void setHasSchoolarship(boolean hasSchoolarship) {
        this.hasSchoolarship = hasSchoolarship;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", grade=" + grade +
                ", email='" + email + '\'' +
                ", hasSchoolarship='" + hasSchoolarship + '\'' +
                '}';
    }
}
