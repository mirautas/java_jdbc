package net.netconomy.jdbc;

import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        
        // ------------------- STUDENT ------------------
        StudentDAO studentDAO = null;

        try {
            studentDAO = new StudentDAO("com.mysql.jdbc.Driver",
                    "jdbc:mysql://localhost/school?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                    "root", "Naeuna07072002!");

            System.out.println("Inserting new student:");
            Student student1 = new Student("John", "Doe", 5, "jd@gmail.com", true);
            System.out.println(student1);
            studentDAO.insertStudent(student1);


            System.out.println("Reading student with id: " + 2);
            Student student2 = studentDAO.getStudent(2);
            System.out.println(student2);

            System.out.println("Reading all students");
            List<Student> students = studentDAO.getAllStudents();
            System.out.println(students);


            System.out.println("Updating student with id: " + 1);
            Student student2Change = studentDAO.getStudent(1);
            if (student2Change != null) {
                System.out.println("Before: " + student2Change);
                student2Change.setHasSchoolarship(false);
                student2Change.setGrade(9);
                studentDAO.updateStudent(student2Change);
                System.out.println("After: " + studentDAO.getStudent(1));
            } else {
                System.out.println("Student with id " + 1 + " does not exist");
            }


            System.out.println("List of students before delete");
            List<Student> students1 = studentDAO.getAllStudents();
            System.out.println(students1);

            System.out.println("Deleting student with id: " + 2);
            studentDAO.deleteStudent(2);

            System.out.println("Deleting student with id: " + 1);
            Student student2Delete = studentDAO.getStudent(1);
            if (student2Delete != null) {
                studentDAO.deleteStudent(student2Delete);
            } else {
                System.out.println("Student with id " + 1 + " does not exist");
            }

            System.out.println("List of students after delete");
            List<Student> students2 = studentDAO.getAllStudents();
            System.out.println(students2);


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            studentDAO.close();
        }


        // ------------------- TEACHER ------------------

        TeacherDAO teacherDAO = null;

        try {
            teacherDAO = new TeacherDAO("com.mysql.jdbc.Driver",
                    "jdbc:mysql://localhost/school?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                    "root", "Naeuna07072002!");

            System.out.println("Inserting new teacher:");
            Teacher teacher1 = new Teacher("Sabine", "Grey", "Science");
            System.out.println(teacher1);
            teacherDAO.insertTeacher(teacher1);


            System.out.println("Reading teacher with id: " + 2);
            Teacher teacher2 = teacherDAO.getTeacher(2);
            System.out.println(teacher2);

            System.out.println("Reading all teachers");
            List<Teacher> teachers = teacherDAO.getAllTeachers();
            System.out.println(teachers);


            System.out.println("Updating teacher with id: " + 1);
            Teacher teacher2Change = teacherDAO.getTeacher(1);
            if (teacher2Change != null) {
                System.out.println("Before: " + teacher2Change);
                teacher2Change.setSubject("Chinese");
                teacherDAO.updateTeacher(teacher2Change);
                System.out.println("After: " + teacherDAO.getTeacher(1));
            } else {
                System.out.println("Teacher with id " + 1 + " does not exist");
            }


            System.out.println("List of teachers before delete");
            List<Teacher> teachers1 = teacherDAO.getAllTeachers();
            System.out.println(teachers1);

            System.out.println("Deleting teacher with id: " + 2);
            teacherDAO.deleteTeacher(2);

            System.out.println("Deleting teacher with id: " + 1);
            Teacher teacher2Delete = teacherDAO.getTeacher(1);
            if (teacher2Delete != null) {
                teacherDAO.deleteTeacher(teacher2Delete);
            } else {
                System.out.println("Teacher with id " + 1 + " does not exist");
            }

            System.out.println("List of teachers after delete");
            List<Teacher> teachers2 = teacherDAO.getAllTeachers();
            System.out.println(teachers2);


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            teacherDAO.close();
        }
    }
}

