package net.netconomy.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {

    private String jdbcDriver;
    private String jdbcUrl;
    private String jdbcUser;
    private String jdbcPassword;
    private Connection connection;


    public StudentDAO(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword) throws SQLException, ClassNotFoundException {
        this.jdbcDriver = jdbcDriver;
        this.jdbcUrl = jdbcUrl;
        this.jdbcUser = jdbcUser;
        this.jdbcPassword = jdbcPassword;
        openConnection();
    }

    private void openConnection() throws ClassNotFoundException, SQLException {
        Class.forName(jdbcDriver);
        connection = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);

    }

    public void close() {

        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteStudent(int id) throws SQLException {

        PreparedStatement ps = connection.prepareStatement("delete from students where id = ?");

        ps.setInt(1, id);
        ps.execute();
        ps.close();
    }

    public void deleteStudent(Student student2Delete) throws SQLException {
        deleteStudent(student2Delete.getId());
    }

    public void insertStudent(Student student) throws SQLException {
        String sql = "INSERT INTO `students`\n" +
                "(\n" +
                "`first_name`,\n" +
                "`last_name`,\n" +
                "`grade`,\n" +
                "`email`,\n" +
                "`hasSchoolarship`)\n" +
                "VALUES\n" +
                "(?,?,?,?,?);\n";

        PreparedStatement ps = connection.prepareStatement(sql);

        int index = 1;

        //replace placeholders ? with data
        ps.setString(index++, student.getFirst_name());
        ps.setString(index++, student.getLast_name());
        ps.setInt(index++, student.getGrade());
        ps.setString(index++, student.getEmail());
        ps.setBoolean(index++, student.getHasSchoolarship());


        ps.execute();
        ps.close();


    }

    public Student getStudent(int id) throws SQLException {

        String sql = "SELECT id,\n" +
                "    first_name,\n" +
                "    last_name,\n" +
                "    grade,\n" +
                "    email,\n" +
                "    hasSchoolarship\n" +
                "FROM students where id = ?;";

        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, id);

        ResultSet rs = ps.executeQuery();

        if (rs.next()) {
            Student student = createStudent(rs);

            rs.close();
            ps.close();

            return student;

        }


        ps.close();
        return null;

    }

    private Student createStudent(ResultSet rs) throws SQLException {
        Student student = new Student();
        student.setId(rs.getInt("id"));
        student.setFirst_name(rs.getString("first_name"));
        student.setLast_name(rs.getString("last_name"));
        student.setGrade(rs.getInt("grade"));
        student.setEmail(rs.getString("email"));
        student.setHasSchoolarship(rs.getBoolean("hasSchoolarship"));
        return student;
    }

    public List<Student> getAllStudents() throws SQLException {
        String sql = "SELECT id,\n" +
                "    first_name,\n" +
                "    last_name,\n" +
                "    grade,\n" +
                "    email,\n" +
                "    hasSchoolarship\n" +
                "FROM students";

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        List<Student> students = new ArrayList<>();

        while (rs.next()) {

            Student student = createStudent(rs);
            students.add(student);
        }

        rs.close();
        stmt.close();

        return students;
    }

    public void updateStudent(Student student) throws SQLException {
        String sql = "UPDATE `students`\n" +
                "SET\n" +
                "`first_name` = ?,\n" +
                "`last_name` = ?,\n" +
                "`grade` = ?,\n" +
                "`email` = ?,\n" +
                "`hasSchoolarship` = ?\n" +
                "WHERE `id` = ?;\n";

        PreparedStatement ps = connection.prepareStatement(sql);


        int index = 1;

        //replace placeholders ? with data
        ps.setString(index++, student.getFirst_name());
        ps.setString(index++, student.getLast_name());
        ps.setInt(index++, student.getGrade());
        ps.setString(index++, student.getEmail());
        ps.setBoolean(index++, student.getHasSchoolarship());


        ps.setInt(index++, student.getId());

        ps.execute();
        ps.close();

    }

    @Override
    public String toString() {
        return "StudentDao{" +
                "jdbcDriver='" + jdbcDriver + '\'' +
                ", jdbcUrl='" + jdbcUrl + '\'' +
                ", jdbcUser='" + jdbcUser + '\'' +
                ", jdbcPassword='" + jdbcPassword + '\'' +
                ", connection=" + connection +
                '}';
    }
}
