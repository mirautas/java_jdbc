package net.netconomy.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeacherDAO {
    private String jdbcDriver;
    private String jdbcUrl;
    private String jdbcUser;
    private String jdbcPassword;
    private Connection connection;


    public TeacherDAO(String jdbcDriver, String jdbcUrl, String jdbcUser, String jdbcPassword) throws SQLException, ClassNotFoundException {
        this.jdbcDriver = jdbcDriver;
        this.jdbcUrl = jdbcUrl;
        this.jdbcUser = jdbcUser;
        this.jdbcPassword = jdbcPassword;
        openConnection();
    }

    private void openConnection() throws ClassNotFoundException, SQLException {
        Class.forName(jdbcDriver);
        connection = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);

    }

    public void close() {

        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void deleteTeacher(int id) throws SQLException {

        PreparedStatement ps = connection.prepareStatement("delete from teachers where id = ?");

        ps.setInt(1, id);
        ps.execute();
        ps.close();
    }

    public void deleteTeacher(Teacher teacher2Delete) throws SQLException {
        deleteTeacher(teacher2Delete.getId());
    }

    public void insertTeacher(Teacher teacher) throws SQLException {
        String sql = "INSERT INTO `teachers`\n" +
                "(\n" +
                "`first_name`,\n" +
                "`last_name`,\n" +
                "`subject`)\n" +
                "VALUES\n" +
                "(?,?,?);\n";

        PreparedStatement ps = connection.prepareStatement(sql);

        int index = 1;

        //replace placeholders ? with data
        ps.setString(index++, teacher.getFirst_name());
        ps.setString(index++, teacher.getLast_name());
        ps.setString(index++, teacher.getSubject());

        ps.execute();
        ps.close();


    }

    public Teacher getTeacher(int id) throws SQLException {

        String sql = "SELECT id,\n" +
                "    first_name,\n" +
                "    last_name,\n" +
                "    subject\n" +
                "FROM teachers where id = ?;";

        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, id);

        ResultSet rs = ps.executeQuery();

        if (rs.next()) {
            Teacher teacher = createTeacher(rs);

            rs.close();
            ps.close();

            return teacher;

        }


        ps.close();
        return null;

    }

    private Teacher createTeacher(ResultSet rs) throws SQLException {
        Teacher teacher = new Teacher();
        teacher.setId(rs.getInt("id"));
        teacher.setFirst_name(rs.getString("first_name"));
        teacher.setLast_name(rs.getString("last_name"));
        teacher.setSubject(rs.getString("subject"));
        return teacher;
    }

    public List<Teacher> getAllTeachers() throws SQLException {
        String sql = "SELECT id,\n" +
                "    first_name,\n" +
                "    last_name,\n" +
                "    subject\n" +
                "FROM teachers";

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        List<Teacher> teachers = new ArrayList<>();

        while (rs.next()) {

            Teacher teacher = createTeacher(rs);
            teachers.add(teacher);
        }

        rs.close();
        stmt.close();

        return teachers;
    }

    public void updateTeacher(Teacher teacher) throws SQLException {
        String sql = "UPDATE `teachers`\n" +
                "SET\n" +
                "`first_name` = ?,\n" +
                "`last_name` = ?,\n" +
                "`subject` = ?\n" +
                "WHERE `id` = ?;\n";

        PreparedStatement ps = connection.prepareStatement(sql);


        int index = 1;

        //replace placeholders ? with data
        ps.setString(index++, teacher.getFirst_name());
        ps.setString(index++, teacher.getLast_name());
        ps.setString(index++, teacher.getSubject());


        ps.setInt(index++, teacher.getId());

        ps.execute();
        ps.close();

    }


    @Override
    public String toString() {
        return "TeacherDAO{" +
                "jdbcDriver='" + jdbcDriver + '\'' +
                ", jdbcUrl='" + jdbcUrl + '\'' +
                ", jdbcUser='" + jdbcUser + '\'' +
                ", jdbcPassword='" + jdbcPassword + '\'' +
                ", connection=" + connection +
                '}';
    }
}
